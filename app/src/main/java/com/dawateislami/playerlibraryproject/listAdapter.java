package com.dawateislami.playerlibraryproject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dawateislami.audioplayer.PlayClass;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class listAdapter extends RecyclerView.Adapter<listAdapter.VeiwHolder> {

    ArrayList<String> list;
    Context context;
    PlayClass play;

    public listAdapter(Context context,ArrayList<String> list,PlayClass play) {

        this.play = play;
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public VeiwHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list, parent, false);
        return new VeiwHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final VeiwHolder holder, final int position) {

        holder.pla_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play.setPlayButtonImage(R.drawable.play);
                play.setPauseButtonImage(R.drawable.pause);
                play.setSeekBarColor(R.color.colorPrimaryDark);
                play.playAudio((Activity) context,holder.pla_btn_container,holder.name,holder.pla_btn,"Test",list.get(position),R.mipmap.ic_launcher);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VeiwHolder extends RecyclerView.ViewHolder {
        ImageView pla_btn;
        TextView total_time;
        SeekBar seekbar;
        TextView name;
        RelativeLayout pla_btn_container;

        public VeiwHolder(@NonNull View itemView) {
            super(itemView);

            pla_btn = itemView.findViewById(R.id.pla_btn);
            total_time = itemView.findViewById(R.id.total_time);
            seekbar = itemView.findViewById(R.id.seekbar);
            name = itemView.findViewById(R.id.name);
            pla_btn_container = itemView.findViewById(R.id.pla_btn_container);

        }
    }
}
