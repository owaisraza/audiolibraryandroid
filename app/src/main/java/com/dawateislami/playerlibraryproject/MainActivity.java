package com.dawateislami.playerlibraryproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dawateislami.audioplayer.Interface.Constants;
import com.dawateislami.audioplayer.PlayClass;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ImageView pla_btn;
    TextView total_time;
    SeekBar seekbar;
    TextView name;

    ImageView pla_btn1;
    TextView total_time1;
    SeekBar seekbar1;
    TextView name1;
    PlayClass play;
    RelativeLayout pla_btn_container;
    ArrayList<String> medialist;
    listAdapter adapter;
    RecyclerView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pla_btn = findViewById(R.id.pla_btn);
        total_time = findViewById(R.id.total_time);
        seekbar = findViewById(R.id.seekbar);
        name = findViewById(R.id.name);
        pla_btn_container = findViewById(R.id.pla_btn_container);

        pla_btn1 = findViewById(R.id.pla_btn1);
        total_time1 = findViewById(R.id.total_time1);
        seekbar1 = findViewById(R.id.seekbar1);
        name1 = findViewById(R.id.name1);

        medialist = new ArrayList<>();
        medialist.add("https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59424.mp3");
        medialist.add("file:///storage/emulated/0/Molana%20Ilyas%20Qadri/Audio/77959.mp3");

        medialist.add("https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59425.mp3");

        medialist.add("https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59426.mp3");

        medialist.add("https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59427.mp3");

        medialist.add("https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59428.mp3");

        play = new PlayClass();



        pla_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                play.setPlayButtonImage(R.drawable.play);
                play.setPauseButtonImage(R.drawable.pause);
//                play.setSeekBarColor(R.color.colorPrimaryDark);
               // play.playAudio(MainActivity.this,pla_btn_container,total_time,name,seekbar,pla_btn,"","https://data2.dawateislami.net/download/tilawat-e-quran/ur/mp3/2018/59424.mp3");
            }
        });

        pla_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // play.playAudio(MainActivity.this,pla_btn_container,total_time1,name1,seekbar1,pla_btn1,"","file:///storage/emulated/0/Molana%20Ilyas%20Qadri/Audio/77959.mp3");
            }
        });

        list = findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));

        adapter = new listAdapter(this,medialist,play);
        list.setAdapter(adapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        play.resetPlayer();
    }
}
