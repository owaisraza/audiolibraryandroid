package com.dawateislami.playerlibraryproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    Button btn;

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("HSn",pref.getString("playing_cat","1"));
        btn.setText(pref.getString("playing_cat","1"));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        btn  = findViewById(R.id.btn);

        pref = getApplicationContext().getSharedPreferences("My Pref", 0);
        editor = pref.edit();

        editor.putString("playing_cat","1").commit();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

    }
}
