package com.dawateislami.audioplayer.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by KFMNH1040 on 10/6/2017.
 */

public abstract class BasePermissionsActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_CODE = 1;

    protected abstract void onPermissionSuccess();

    public void requestPermission(Context context) {
        if(!checkPermission(context))
            ActivityCompat.requestPermissions((Activity) context, new String[]
                    {
                            READ_EXTERNAL_STORAGE,
                            WRITE_EXTERNAL_STORAGE
                    }, REQUEST_PERMISSION_CODE);
        else
            onPermissionSuccess();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean readstoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writestoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (readstoragePermission && writestoragePermission ) {
                        Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                        onPermissionSuccess();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                        this.finish();
                    }
                }
                break;
        }
    }

    public boolean checkPermission(Context context) {
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE);
        return
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED;
    }
}
