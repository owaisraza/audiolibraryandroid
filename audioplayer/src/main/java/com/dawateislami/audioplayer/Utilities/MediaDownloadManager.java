package com.dawateislami.audioplayer.Utilities;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;


import java.io.File;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MediaDownloadManager {

    private static DownloadManager downloadManager;

    public static void init(Context context) {

        downloadManager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
    }

    public static long startDownload(String title, String url, String path) {

        long downloadReference = -1;

        Uri source = Uri.parse(url);
       // Uri source = Uri.parse("http://data2.dawateislami.net/download/short-clips/ur/mp3/2016/39424.mp3");

        Uri destination = Uri.fromFile(new File(path));

        DownloadManager.Request request = new DownloadManager.Request(source);

        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);

        //Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);

        request.setVisibleInDownloadsUi(true);

        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle(title);

        //Set a description of this download, to be displayed in notifications (if enabled)
        //       request.setDescription("Android Data download using DownloadManager.");

        //Set the local destination for the downloaded file to a path within the application's external files directory
//        request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS,"CountryList.json");
        request.setDestinationUri(destination);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        //Enqueue a new download and same the referenceId
        downloadReference = downloadManager.enqueue(request);

        //     sharedPreferences.edit().putLong(title, downloadReference).commit();

        return downloadReference;
    }

    public static void displayAllDownloads() {
/*
        Intent intent = new Intent();
        intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        context.startActivity(intent);
*/
    }



    public static void cancelDownload(long downloadReference) {

        downloadManager.remove(downloadReference);
           //   sharedPreferences.edit().remove(title).commit();
    }

/*
    private static BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

        //    if (downloadReference == referenceId){


        //    }
        }
    };
    */
}