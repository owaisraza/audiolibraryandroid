package com.dawateislami.audioplayer.Utilities;

import android.content.Context;
import android.os.Environment;


import java.io.File;
import java.util.ArrayList;

public class SDCardManager {

    public static boolean isSdCardReadable() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static boolean isSdCardWritable() {

        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWritable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {

            // We can read and write the media
            mExternalStorageAvailable = true;
            mExternalStorageWritable = true;

        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {

            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWritable = false;

        } else {

            // Something else is wrong. It may be one of many other states,
            // but all we need to know is we can neither read nor write
            mExternalStorageAvailable = false;
            mExternalStorageWritable = false;
        }

        if ((mExternalStorageAvailable) && (mExternalStorageWritable)) {
            return true;
        } else {
            return false;
        }
    }

    private static String getFormattedTitle (String title) {

//        return title.replaceAll("[^a-zA-Z0-9. ]", "");
        return title.replaceAll("[^a-zA-Z0-9.-]", "_");
    }




//    public static boolean updatePdfFile(String oldTitle, String newTitle) {
//
//        boolean success = false;
//
//        String oldPath = getPdfPath(oldTitle);
//
//        File oldFile = new File(oldPath);
//
//        if (oldFile.isFile()) {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
//                oldFile.setWritable(true);
//            }
//
//            String newPath = getPdfPath(newTitle);
//            File newFile = new File(newPath);
//            success = oldFile.renameTo(newFile);
//        }
//
//        return success;
//    }

    private static void makeDirs (String path) {

        File folder = new File(path);

        if (folder.isDirectory()) {
            // do nothing...
        } else {
            folder.mkdirs() ;
        }
    }

    public static boolean deleteDirs(String path) {

        File file = new File(path);

        if (file.isDirectory()) {

            String[] children = file.list();

            for (int i = 0; i < children.length; i++) {

                boolean success = deleteDirs(new String(file + "/" + children[i]));

                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return file.delete();
    }


}
