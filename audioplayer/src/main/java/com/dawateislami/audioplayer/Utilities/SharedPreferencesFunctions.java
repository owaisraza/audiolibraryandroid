package com.dawateislami.audioplayer.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by KFMNH1040 on 10/3/2017.
 */

public class SharedPreferencesFunctions {

    private static final String FIRST_RUN_KEY               = "first_run";
    private static final String APPLICATION_LANGUAGE_KEY    = "language";
    private static final String PREF_NAME                   = "weekly_booklet";
    private static final String IS_SEARCH                   = "is_search";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }



    public static String getLanguage(Context context){
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String value = sharedPreferences.getString(APPLICATION_LANGUAGE_KEY, null);
        return value;
    }

    public static void setLanguage(Context context, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(APPLICATION_LANGUAGE_KEY, value);
        editor.apply();
    }

    public static void setIsSearch(Context context, String value) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(IS_SEARCH, value);
        editor.apply();
    }
    public static String getISsearch(Context context){
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String value = sharedPreferences.getString(IS_SEARCH, "false");
        return value;
    }

    public static boolean setPreference(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getPreference(Context context, String key, String Default) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, Default);
    }

    public static boolean unsetPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        return editor.commit();
    }

    public static Boolean isPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return settings.contains(key);
    }


    /*******************************************************************/


    public static boolean setLongPreference(Context context, String key, long value) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static boolean unsetLongPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        return editor.commit();
    }

    public static long getLongPreference(Context context, String key, long Default) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return settings.getLong(key, Default);
    }

    public static boolean isLongPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return settings.contains(key);
    }


    public static void setBindService(Context context,String key,String value) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key,value);
        editor.commit();
    }
    public static String getBindService(Context context, String key, long Default) {
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return settings.getString(key,"");
    }
}
