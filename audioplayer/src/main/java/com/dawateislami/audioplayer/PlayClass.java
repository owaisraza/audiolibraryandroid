package com.dawateislami.audioplayer;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.provider.SyncStateContract;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dawateislami.audioplayer.AudioPlayer.AudioNotificationService;
import com.dawateislami.audioplayer.Interface.Constants;
import com.dawateislami.audioplayer.Utilities.BasePermissionsActivity;
import com.dawateislami.audioplayer.Utilities.Helper;
import com.dawateislami.audioplayer.Utilities.SharedPreferencesFunctions;

import org.w3c.dom.Text;

import androidx.appcompat.app.AppCompatActivity;

public class PlayClass extends BasePermissionsActivity {

    TextView audioname ;
    SeekBar seekBar;
    Helper helper;
    ImageView playBtn;
    private AudioNotificationService audioService;
    public static boolean playPause = false;
    private boolean intialStage = true;
    TextView totaltime;
    String mediaurl;
    Activity context;
    RelativeLayout pla_btn_container;
    int playbtnimg=0;
    int pausebtnimg=0;
    int seekbarcolor=0;
    String headingName;
    Class player;
    int img;
    public PlayClass() {

    }


    public void init(Activity context, TextView totaltime, TextView audioname, SeekBar seekBar, ImageView playBtn){
        this.audioname = audioname;
        this.context = context;
        this.totaltime = totaltime;
        this.playBtn = playBtn;
        this.seekBar = seekBar;
        this.helper = new Helper(context);
    }
    public void init(Activity context,  TextView audioname, ImageView playBtn){
        this.audioname = audioname;
        this.context = context;
        this.playBtn = playBtn;
        this.helper = new Helper(context);
    }

    public void playAudio(Activity context, RelativeLayout progresscontainer, TextView totaltime, TextView audioname, SeekBar seekBar, ImageView playBtn, String headingName, String url,int notification_img){
        init(context,totaltime,audioname,seekBar,playBtn);
        this.pla_btn_container = progresscontainer;
        this.img = notification_img;
        this.mediaurl = url;
        this.headingName = headingName;
        if(url.contains("https")||url.contains("http")){
            connectToServce();

        }
        else {
            requestPermission(context);
        }
    }


    public void playAudio(Activity context, RelativeLayout progresscontainer, TextView audioname, ImageView playBtn, String headingName, String url,int notification_img){
        init(context,audioname,playBtn);
        this.pla_btn_container = progresscontainer;
        this.img = notification_img;
        this.mediaurl = url;
        this.headingName = headingName;
        if(url.contains("https")||url.contains("http")){
            connectToServce();

        }
        else {
            requestPermission(context);
        }
    }

    private void connectToServce() {
        if(mediaurl.contains("https")||mediaurl.contains("http")) {
            if (helper.isOnline()) {

                playMedia();

            }
            else showToast(getResources().getString(R.string.no_internet));
        } else {
            playMedia();
        }
    }

    private void playMedia() {
        if (isMyServiceRunning(AudioNotificationService.class)) {
            if(SharedPreferencesFunctions.getPreference
                    (context, Constants.MEDIA_CURRENT_LINK,"").equals(mediaurl)){
                pausePlay(mediaurl);
                //initializeService(mediaurl,Constants.MEDIA_ACTION_PLAY);
            }else{
                if(audioService!=null) {
                    audioService.hideProgess();
                    if(isMediaPlaying()){
                        audioService.finishPlayer();


                    }
                }

                try{
                    context.unbindService(audioConnection);
                }
                catch (Exception ae){

                }
                initializeService(mediaurl,Constants.MEDIA_ACTION_PLAY);
            }
        } else {
            if(audioService!=null) {
                audioService.hideProgess();
                if(isMediaPlaying()){
                    audioService.finishPlayer();
                }
            }

            try{
                context.unbindService(audioConnection);
            }
            catch (Exception ae){

            }
            initializeService(mediaurl,Constants.MEDIA_ACTION_PLAY);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isMediaPlaying(){
        if(audioService.isPlaying())
            return true;
        else
            return false;
    }
    private void showToast(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }

    public void pausePlay(String path) {
        if(audioService!=null) {
            if(seekBar!=null) {
                audioService.setSeekBar(seekBar);
            }
            setTotalTime();
            audioService.setPlayButton(playBtn);
        }
        Intent playIntent = new Intent(context, AudioNotificationService.class);
        playIntent.setAction(Constants.MEDIA_ACTION_START);
        playIntent.putExtra(Constants.MEDIA_URL,path);
        //playIntent.putExtra(Constants.MEDIA_ID,bookId);
        context.startService(playIntent);
        //playBtn.setImageResource(R.drawable.play_default);
    }

    public  boolean checkPref(String key){

        if(SharedPreferencesFunctions.isPreference(context,key) )
            return true;
        else
            return false;
    }
    private void initializeService(final String path, final String type) {




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent playIntent = null;
                playIntent = new Intent(context, AudioNotificationService.class);
                playIntent.setAction(type);
                playIntent.putExtra(Constants.MEDIA_URL,path);
                audioname.setText(headingName);
              //  playIntent.putExtra(Constants.MEDIA_ID,bookId);
//                boolean isBound = context.bindService( new Intent(context, AudioNotificationService.class), audioConnection, Context.BIND_AUTO_CREATE );
//
//                if (isBound) {
//                    context.unbindService(audioConnection);
//
//                }
//                else {
//
//                }
                context.bindService(playIntent, audioConnection, BIND_AUTO_CREATE);
                context.startService(playIntent);

                //playBtn.setImageResource(R.drawable.pause_default);

                if(audioService!=null) {
                    if(seekBar!=null) {
                        audioService.setSeekBar(seekBar);
                    }
                    setTotalTime();
                    audioService.setPlayButton(playBtn);
                }
            }
        },0);

    }

    private void setTotalTime() {
        if(totaltime!=null) {
            audioService.setTimeCompleted(totaltime);
        }
    }

    public void setPlayButtonImage(int playbtnimg){

        this.playbtnimg =playbtnimg;

    }
    public void setPauseButtonImage(int pauseButton){

        this.pausebtnimg= pauseButton;
    }

    public void setSeekBarColor(int seekBarColor){

        this.seekbarcolor = seekBarColor;
    }


    private ServiceConnection audioConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioNotificationService.MusicBinder binder = (AudioNotificationService.MusicBinder)service;
            audioService = binder.getService();


            audioService.setPauseButton(pausebtnimg);
            audioService.setPlayButtonImage(playbtnimg);
            if(seekBar!=null) {

                audioService.setSeekBarColor(seekbarcolor);
            }
//            audioService.setTitle(headingName);
            audioService.setContaxt(context);
            audioService.setProgressContainer(pla_btn_container);
            audioService.setNotificationHeaderAndImage(headingName,img);


            if(isMediaPlaying()){
                if(pausebtnimg!=0){
                    playBtn.setImageResource(pausebtnimg);
                }
                else {
                    playBtn.setImageResource(R.drawable.pause_default);
                }
            }else{
                if(playbtnimg!=0){
                    playBtn.setImageResource(playbtnimg);

                }
                else {
                    playBtn.setImageResource(R.drawable.play_default);
                }
            }
            if(playPause)
                if(seekBar!=null) {
                    seekBar.setMax(audioService.player.getDuration());
                }

//            download_book_audio_link = audioResala.getAudio_link();
//            download_book_audio_name =  audioResala.getRomanName() + "_"+ audioResala.getAudio_locale() + Constants.MP3;
//            String path = SDCardManager.getMediaPath(download_book_audio_name, Constants.MEDIA_PATH_MP3);
            String ref = SharedPreferencesFunctions.getPreference(context,Constants.MEDIA_CURRENT_LINK,"");
            if(ref.equals(mediaurl))
            {
                intialStage = false;
                if(seekBar!=null) {
                    audioService.setSeekBar(seekBar);
                }

                setTotalTime();
                audioService.setPlayButton(playBtn);

            }

            else
            {
                intialStage = false;
                if(seekBar!=null) {
                    audioService.setSeekBar(seekBar);
                }
                setTotalTime();
                audioService.setPlayButton(playBtn);
                if(playbtnimg!=0){
                    playBtn.setImageResource(playbtnimg);

                }
                else {
                    playBtn.setImageResource(R.drawable.play_default);
                }
            }


            Log.e("START_SERVICE","Audio Service Bound: " );
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e("STOP_SERVICE","Audio Service Bound: " );
        }
    };


    @Override
    protected void onPermissionSuccess() {
        connectToServce();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        resetPlayer();

    }

    public  void resetPlayer(){
        SharedPreferencesFunctions.unsetPreference(context,Constants.MEDIA_CURRENT_LINK);
        context.unbindService(audioConnection);
    }
}
