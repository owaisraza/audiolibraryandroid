package com.dawateislami.audioplayer.AudioPlayer;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.dawateislami.audioplayer.Interface.Constants;
import com.dawateislami.audioplayer.PlayClass;
import com.dawateislami.audioplayer.R;
import com.dawateislami.audioplayer.Utilities.Helper;
import com.dawateislami.audioplayer.Utilities.SharedPreferencesFunctions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


/**
 * Created by KFMMH809 on 5/11/2017.
 */

public class AudioNotificationService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener {

    NotificationCompat.Builder status ;
    NotificationManager mNotificationManager;
    private String[] channelNm = new String[]{"", ""};
    private String[] channelAdd = new String[]{"", ""};
    String url;
    int id;
    private final IBinder musicBind = new MusicBinder();

    private final String LOG = "AudioService";
    private AudioManager mAudioManager;
    private static final String RADIO_CHANNEL_ID = "com.dawateislami.OnlineIslamicBooks";

    private SeekBar seekBar;
    private TextView timeCompleted;
    private TextView totalTime;
    //private DatabaseHelper db;
    ScheduledExecutorService thread;
    Handler handler;
    Runnable task1;
    String media_name="";
    Activity Class_name;
    ImageView play_btn;
    //media player
    public MediaPlayer player;
    int playbtnimg=0;
    int pausebtnimg=0;
    int seekbarcolor=0;
    RelativeLayout pla_btn_container;
    ProgressBar progressBar;
    String title;
    Context context_class;
    int img;

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    public AudioNotificationService() {

    }

    public AudioNotificationService(String name) {
        this.media_name = name;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        //initialize



        initMusicPlayer();
        //db = new DatabaseHelper(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.release();
        handler.removeCallbacks(task1);
        if(thread != null)
            thread.shutdown();
        player = null;
    }

    //activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }


    //release resources when unbind
    @Override
    public boolean onUnbind(Intent intent) {
        /*player.stop();
        player.release();*/
        return false;
    }



    public void initMusicPlayer() {
        //set player properties

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        player = new MediaPlayer();
        handler = new Handler();

        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        pref = getApplicationContext().getSharedPreferences("My Pref", 0);
        editor = pref.edit();


        //set listeners


        player.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {


            @Override
            public void onBufferingUpdate(MediaPlayer what, int i) {

               Log.d("HSN",i+"");
            }



        });
        player.setOnInfoListener(new MediaPlayer.OnInfoListener() {

            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START)
                    showProgess();
                if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END)
                    hideProgess();

                return false;
            }
        });

    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {

            if(intent.getStringExtra(Constants.MEDIA_URL) != null){
                url = intent.getStringExtra(Constants.MEDIA_URL);
//                id = intent.getIntExtra(Constants.MEDIA_ID,0);
                SharedPreferencesFunctions.setPreference(this,Constants.MEDIA_CURRENT_LINK,url);
            }

            if(intent.getAction() != null) {

                switch (intent.getAction()) {
                    case Constants.MEDIA_ACTION_PLAY:
                        removeNotifcation();
                        registerReceiver(mMessageReceiver1, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        try {
                            player.reset();
                        }
                        catch (Exception ae){

                        }

                        try {
                          playUrl();
                        } catch (Exception e) {
                            Log.e("MUSIC SERVICE", "Error setting data source", e);
                        }
                        Intent play = new Intent(Constants.MEDIA_BROADCAST_RECIVER);
                        play.putExtra(Constants.MEDIA_STATE_ACTION,Constants.MEDIA_ACTION_PLAY);
                        sendBroadcast(play);
                        break;

                    case Constants.MEDIA_ACTION_PAUSE:
                        try {

                            unregisterReceiver(mMessageReceiver1);
                        }
                        catch (Exception ae)
                        {

                        }
                        Intent pause = new Intent(Constants.MEDIA_BROADCAST_RECIVER);
                        pause.putExtra(Constants.MEDIA_STATE_ACTION,Constants.MEDIA_ACTION_PAUSE);
                        sendBroadcast(pause);
                        player.pause();
                        if(playbtnimg==0){
                            play_btn.setImageResource(R.drawable.play_default);

                        }
                        else {
                            play_btn.setImageResource(playbtnimg);
                        }

                        Log.i(LOG, "Clicked Play");

                        break;

                    case Constants.MEDIA_ACTION_START:
                        Intent start = new Intent(Constants.MEDIA_BROADCAST_RECIVER);
                        registerReceiver(mMessageReceiver1, new IntentFilter(
                                ConnectivityManager.CONNECTIVITY_ACTION));
                        if(player.isPlaying()){
                            player.pause();
                            if(playbtnimg==0){
                               // play_btn.setImageResource(0);
                                play_btn.setImageResource(R.drawable.play_default);

                            }
                            else {
                                play_btn.setImageResource(playbtnimg);
                            }

                            start.putExtra(Constants.MEDIA_IS_PALYED,false);
                        }else{
                            player.start();
                           // showProgess();
                            if(pausebtnimg==0){
                                play_btn.setImageResource(R.drawable.pause_default);
                            }
                            else {
                                play_btn.setImageResource(pausebtnimg);

                            }

                            start.putExtra(Constants.MEDIA_IS_PALYED,true);
                        }
                        start.putExtra(Constants.MEDIA_STATE_ACTION,Constants.MEDIA_ACTION_START);
                        sendBroadcast(start);
                        break;

                    case Constants.MEDIA_ACTION_STOP:
                        Log.i(LOG, "Received Stop Foreground Intent");
                        finishPlayer();
                        unregisterReceiver(mMessageReceiver1);
                        SharedPreferencesFunctions.unsetPreference(context_class,Constants.MEDIA_CURRENT_LINK);
                        break;
                    case Constants.MEDIA_ACTION_NO:
                        break;
                }
            }



            return START_STICKY;
        } catch (Exception ae) {

//            initMusicPlayer();
//            try {
//                playUrl();
//            } catch (Exception e) {
//                Log.e("MUSIC SERVICE", "Error setting data source", e);
//            }
            Log.d("onStartCommand",ae.toString());
           // AudioPlayerActivity.hideDialog();
            return START_STICKY;

        }
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);

    }

    public void setNotificationHeaderAndImage(String name,int img){
        this.media_name = name;
        this.img = img;

    }
    public void finishPlayer(){
        player.reset();
        editor.putString("playing_cat","").commit();


       // player.release();
        if(playbtnimg==0){
            play_btn.setImageResource(R.drawable.play_default);
        }
        else {
            play_btn.setImageResource(playbtnimg);
       }
        SharedPreferencesFunctions.unsetPreference(context_class,Constants.MEDIA_CURRENT_LINK);

        stopSelf();


        Intent stop = new Intent(Constants.MEDIA_BROADCAST_RECIVER );
        stop.putExtra(Constants.MEDIA_STATE_ACTION,Constants.MEDIA_ACTION_STOP);
        sendBroadcast(stop);
      //  SharedPreferencesFunctions.unsetPreference(this,Constants.MEDIA_CURRENT_LINK);
        removeNotifcation();

        stopService(new Intent(getApplicationContext(),AudioNotificationService.class));
    }

    private void removeNotifcation() {
        stopForeground(true);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                mNotificationManager.cancel(Constants.FOREGROUND_SERVICE);
        }
        catch (Exception ae)
        {

        }
    }


    private void showNotification() {
        RemoteViews bigViews = new RemoteViews(getPackageName(), R.layout.status_bar_audio_notification);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        bigViews.setTextViewText(R.id.status_bar_track_name, media_name);
        bigViews.setImageViewResource(R.id.notification_img,img);
        if (Build.VERSION.SDK_INT < 20) {
            bigViews.setTextColor(R.id.status_bar_track_name, Color.WHITE);
        }
        Intent notificationIntent = null;
        try {
            notificationIntent = new Intent(this, Class.forName(context_class.getClass().getName()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        notificationIntent.putExtra("book_in_lang_id",id);
        notificationIntent.putExtra("audio_url",url);
        notificationIntent.setAction(Constants.MAIN_ACTION);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        int pendingIntentFlag = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ?
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE :
                PendingIntent.FLAG_UPDATE_CURRENT;

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(), pendingIntentFlag);


        Intent playIntent = new Intent(this, AudioNotificationService.class);
        playIntent.setAction(Constants.MEDIA_ACTION_START);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, pendingIntentFlag);


        Intent closeIntent = new Intent(this, AudioNotificationService.class);
        closeIntent.setAction(Constants.MEDIA_ACTION_STOP);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0,
                closeIntent, pendingIntentFlag);

        Intent pauseIntent = new Intent(this, AudioNotificationService.class);
        pauseIntent.setAction(Constants.MEDIA_ACTION_PAUSE);
        PendingIntent ppauseIntent = PendingIntent.getService(this, 0,
                pauseIntent, pendingIntentFlag);

//        bigViews.setOnClickPendingIntent(R.id.parent,pplayIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);
        bigViews.setOnClickPendingIntent(R.id.status_bar_pause, ppauseIntent);

        NotificationChannel mChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
           // CharSequence name = getString(R.string.islamic_book);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            mChannel = new NotificationChannel(RADIO_CHANNEL_ID, "Test", importance);
        }

        /*Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);*/

        status = new NotificationCompat.Builder(this, RADIO_CHANNEL_ID);
        status.setCustomBigContentView(bigViews);
        status.setContent(bigViews);
//        status.setSound(null,0);
        status.setPriority(NotificationCompat.PRIORITY_HIGH);
        status.setOngoing(true);

        //status = NotificationCompat.FLAG_ONGOING_EVENT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            status.setSmallIcon(img);
        else
            status.setSmallIcon(img);

        status.setContentIntent(pendingIntent);

//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//        notificationManager.notify(, status.build());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel( mChannel);
        }
        startForeground(Constants.FOREGROUND_SERVICE, status.build());

    }



    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.reset();
        Log.d("onCompletion","finish");
        finishPlayer();
        //AudioPlayerActivity.hideDialog();

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();
      //  AudioPlayerActivity.hideDialog();
        Log.d("onError",""+i + "      "+ i1);
        return false;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        try {

            if (focusChange == 0) {
                unregisterReceiver(mMessageReceiver1);
                Intent pause = new Intent(Constants.MEDIA_BROADCAST_RECIVER);
                pause.putExtra(Constants.MEDIA_STATE_ACTION,Constants.MEDIA_ACTION_PAUSE);
                sendBroadcast(pause);
                player.pause();
            } else {
                //player.start();
            }
        }catch (Exception e){
            Log.d("FocusChange",e.toString());
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        hideProgess();
        if(pausebtnimg==0){
            play_btn.setImageResource(R.drawable.pause_default);

        }
        else {
            play_btn.setImageResource(pausebtnimg);
        }
    //    AudioPlayerActivity.hideDialog();
        updateTotalTime();
        if(seekBar!=null) {
            updateSeekBar();
        }
        showNotification();
    }





    private void playUrl() {

        try {
            Uri audioUri = null;
            if (URLUtil.isValidUrl(url)) {
                audioUri = Uri.parse(url);
            }else{
                audioUri = Uri.fromFile(new File(this.url));
            }

            if(audioUri != null) {
                if (player != null) {
                    player.reset();
                    player.setDataSource(this,audioUri);
                    player.setOnPreparedListener(this);
                    player.setOnCompletionListener(this);
                    player.setOnErrorListener(this);

                    if (URLUtil.isValidUrl(url))
                        player.prepareAsync();
                    else player.prepare();

                }else{
                    showToast("player not initialize");
                }
            }else{
                showToast("url is empty");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("HSN error","error "+e.getMessage().toString());
           // AudioPlayerActivity.hideDialog();
        }



    }


    public boolean isPlaying(){

        if(player != null) {
            try {
                return player.isPlaying();
            } catch (Exception ae) {

            }
        }
        else
            return false;
        return false;
    }

    public void pausePlayer(){
        player.pause();
    }

    public void seek(int posn){
        player.seekTo(posn);
    }

    public void playPlayer(){
        player.start();
    }

    public void onRelease(){
        player.release();
    }

    public void stopPlayer() {
        player.reset();
    }

    public String getAudioId() {
        return this.url;
    }

    public void updateTotalTime() {
        if(null != totalTime && -1 != player.getDuration()){
            final int timeArray[] = {0,1,2};
            convertMillisecondsToHoursAndMinutes(player.getDuration(),timeArray);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    totalTime.setText(String.format("%02d:%02d", new Object[]{Integer.valueOf(Math.abs(timeArray[1])), Integer.valueOf(Math.abs(timeArray[2]))}));
                }
            });
        }
    }

    public void updateSeekBar(){

        if(null != thread)
            thread.shutdownNow();

        if(null != seekBar && -1 != player.getDuration()){
            seekBar.setMax(player.getDuration());

            final int total = player.getDuration();
           // seekBar.setMax(total);

            task1 = new Runnable() {
                int currentPosition ;
                @Override
                public void run() {

                    try {
                         currentPosition = player.getCurrentPosition();
                    }
                    catch (Exception ae)
                    {
                        Log.d("SeekBar","getCurrentposition");
                    }
                    Log.d("SeekBar", String.valueOf(currentPosition));
                    if(player != null && currentPosition < total) {
                        seekBar.setProgress(currentPosition);

                        if(null != timeCompleted && -1 != currentPosition){
                            final int timeArray[] = {0,1,2};
                            convertMillisecondsToHoursAndMinutes(currentPosition,timeArray);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(currentPosition <= 0 )
                                    {
                                        seekBar.setProgress(0);
                                        timeCompleted.setText("00:00:00");
                                        handler.removeCallbacks(task1);
                                    }
                                    else

                                    {
                                        timeCompleted.setText(String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf(Math.abs(timeArray[0])), Integer.valueOf(Math.abs(timeArray[1])), Integer.valueOf(Math.abs(timeArray[2]))}));
                                    }
                                }
                            });
                        }
                    }
                }
            };
            thread = Executors.newScheduledThreadPool(1);
            thread.scheduleAtFixedRate(task1, 0, 1, TimeUnit.SECONDS);




            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(fromUser && null != player)
                    {
                        int total = player.getDuration();
                        if(progress < total)
                            player.seekTo(progress);

                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    //Do Nothing....
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    //Do Nothing....
                }
            });
        }
    }

    public SeekBar getSeekBar() {
        return seekBar;
    }

    public void setSeekBar(SeekBar seekBar) {
//        if(null != this.seekBar)
//            this.seekBar.setOnSeekBarChangeListener(null);
        this.seekBar = seekBar;
        if(seekbarcolor!=0){
            seekBar.getProgressDrawable().setColorFilter(seekbarcolor, PorterDuff.Mode.SRC_ATOP);
            seekBar.getThumb().setColorFilter(seekbarcolor, PorterDuff.Mode.SRC_ATOP);


        }

        if(isPlaying())
            if(seekBar!=null) {
                updateSeekBar();
            }
    }

    public void setPlayButtonImage(int playbtnimg){

        this.playbtnimg = playbtnimg;
    }
    public void setPauseButton(int pauseButton){
        this.pausebtnimg=pauseButton;
    }

    public void setSeekBarColor(int seekBarColor){
        this.seekbarcolor =seekBarColor;



    }

    public TextView getTimeCompleted() {
        return timeCompleted;
    }

    public void setTimeCompleted(TextView timeCompleted) {
        this.timeCompleted = timeCompleted;
    }
    public void setPlayButton(ImageView playbtn){
        this.play_btn =  playbtn;
    }
    public void setTextView(TextView timeCompleted)
    {
        this.timeCompleted = timeCompleted;
    }

    public void setTotalTime(TextView totalTime) {
        this.totalTime = totalTime;
        updateTotalTime();
    }

    public TextView getTotalTime() {
        return totalTime;
    }


    private void convertMillisecondsToHoursAndMinutes(long milliseconds, int[] arg)
    {
        arg[2] = (int) (milliseconds / 1000) % 60 ; //Seconds
        arg[1] = (int) ((milliseconds / (1000*60)) % 60); //Minutes
        arg[0]   = (int) ((milliseconds / (1000*60*60)) % 24); //Hours
    }


    private BroadcastReceiver mMessageReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
           Helper helper = new Helper(getApplicationContext());
            switch (action) {
                case ConnectivityManager.CONNECTIVITY_ACTION:

                    if (!helper.isOnline()) {
//                        stopForeground(true);
//                        stopSelf();
//                        player.reset();
//                        player.release();
                        if(url.contains("http"))
                        {
                            finishPlayer();
                        }


                        unregisterReceiver(mMessageReceiver1);

                    }
                    break;
            }
        }

    };

    public void setProgressContainer(RelativeLayout pla_btn_container) {
        this.pla_btn_container = pla_btn_container;
        progressBar = new ProgressBar(this);
//        progressBar.getIndeterminateDrawable().setColorFilter(
//                getResources().getColor(seekbarcolor), PorterDuff.Mode.SRC_ATOP);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);


        progressBar.setLayoutParams(params);
        pla_btn_container.addView(progressBar);

    }

    public void setTitle(String headingName) {
        this.title = headingName;
    }

    public void setContaxt(Context context) {
        this.context_class = context;
    }

    public class MusicBinder extends Binder {
        public AudioNotificationService getService() {
            return AudioNotificationService.this;
        }
    }

    private void showToast(String msg){
        Toast.makeText(this,msg, Toast.LENGTH_SHORT).show();
    }

    private void showProgess(){
       // play_btn.setVisibility(View.GONE);
        if(progressBar!=null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgess(){
      //  play_btn.setVisibility(View.VISIBLE);
        if(progressBar!=null) {
            progressBar.setVisibility(View.GONE);
        }
    }

}