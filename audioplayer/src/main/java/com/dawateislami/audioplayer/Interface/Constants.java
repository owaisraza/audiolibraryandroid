package com.dawateislami.audioplayer.Interface;

import android.os.Environment;

/**
 * Created by KFMZA903 on 2/28/2017.
 */

public interface Constants {

     String DBBOOK = "databases/MadaniBookLibrary.db";
     String DBPAMPHLETS= "databases/MadaniPamphletsLibrary.db";
     String DBPATH="data/data/com.dawateislami.madanibooklibrary/databases/";
     String DOWNLOAD_URL_BOOK = "https://data2.dawateislami.net/Data/Books/Download/";
     String DOWNLOAD_URL_PAMPHELLTS = "https://data2.dawateislami.net/Data/Pamphlets/Download/";
     String BOOK_COVER = "https://data2.dawateislami.net/Data/Books/Read/";
	 String PAMPHLETS_COVER = "https://data2.dawateislami.net/Data/Pamphlets/Read/";
     String URL_SERVICE_LOCAL = "http://172.16.100.137:8090/wsbookslibrary/";
     String URL_SERVICE_STAGE = "https://stage.dawateislami.org/wsbookslibrary/";
     String URL_SERVICE_LIVE = "https://dawateislami.net/wsbookslibrary/";
     String URL_SERVICE_BOOK = URL_SERVICE_LIVE;
     String URL_SERVICE_PAMPHLET = "https://www.dawateislami.net/wspamphletslibrary/";
     String URL_AUDIO_BOOK = URL_SERVICE_LIVE  + "bookonlinereadheadingaudio?id=";
     String AUDIO_BOOK_DATE_PREFIX = "&sync_datatime=";
     String AUDIO_BOOK_PAGE_PREFIX = "&pn=";
     String AUDIO_BOOK_OFFSET_PREFIX = "&ps=";
     String PREFEX_BOOK_LIBRARY = "&pn=0&ps=500";

    String SERVICE_BROADCAST_ACTION = "action_service";
    String BOOLEAN_BROADCAST_ACTION = "boolean_action_service";





    int RequestPermissionCode = 1;
    int JOB_ID = 1;

     String SD_CARD                  = Environment.getExternalStorageDirectory().getPath();
     String APP_FOLDER              = "Epamphlets";
     String APP_FOLDER2             = "Ebooks";
     String THUMBNAIL_FOLDER         = "Thumbnail";
     String PDF_FOLDER              = "PDF";
     String MEDIA_FOLDER            = "MadaniBookLibrary";
     String IMAGE_FOLDER            = "Gallery";


     String EXTERNAL_DATA_PATH       = SD_CARD ;
     String IMAGE_DATA_PATH          = EXTERNAL_DATA_PATH + "/" + IMAGE_FOLDER;

    //     String THUMBNAIL_DATA_PATH      = EXTERNAL_DATA_PATH + "/" + THUMBNAIL_FOLDER;
     String PDF_DATA_PATH            = EXTERNAL_DATA_PATH + "/" + PDF_FOLDER;
     String EPAMPHLETS_PATH            = EXTERNAL_DATA_PATH + "/" + MEDIA_FOLDER + "/" + APP_FOLDER;
     String EBOOK_PATH            = EXTERNAL_DATA_PATH + "/" + MEDIA_FOLDER + "/" + APP_FOLDER2;

     String STATUS_CURRENT 				= "status_current"; // not final
     String STATUS_READY 			= "status_ready";
     String STATUS_COMPLETE 			= "status_complete";
     String STATUS_CANCEL 			= "status_cancel";
     String STATUS_ERROR 			= "status_error";
     String STATUS_NO_CONNECTION 	= "status_no_connection";
     String STATUS_FAILED            = "status_failed";
     String STATUS_PAUSED            = "status_paused";
     String STATUS_PENDING           = "status_pending";
     String STATUS_RUNNING           = "status_running";
     String STATUS_SUCCESSFUL        = "status_successful";
     String STATUS_EMPTY             = "status_empty";
     String SD_CARD_NOT_AVAILABLE 	= "sd_card_not_available";
     String MEDIA_AVAILABLE           = "media_available" ;
     String MEDIA_NOT_AVAILABLE       = "media_not_available";

    String MEDIA_ACTION_PLAY = "play_audio";
    String MEDIA_ACTION_PAUSE = "pause";
    String MEDIA_ACTION_START = "start";
    String MEDIA_ACTION_STOP = "stop";
    String MEDIA_ACTION_NO = "no";
    String MEDIA_CURRENT_LINK = "media_current_link";
    String BIND_SERVICE = "bind_service";

    String MEDIA_BROADCAST_RECIVER = "play_pause";
    String MEDIA_IS_PALYED = "is_playing";
    String MEDIA_STATE_ACTION = "action";
    String SERVICE_STATE_ACTION = "action_service";
    String BOOLEAN_STATE_ACTION  = "boolean_action_service";
    String MEDIA_URL = "media_url";
    String MEDIA_ID = "media_id";

    String MAIN_ACTION = "com.marothiatechs.customnotification.action.main";
    String INIT_ACTION = "com.marothiatechs.customnotification.action.init";
    String PAUSE = "com.marothiatechs.customnotification.action.pause";
    String PLAY_ACTION = "com.marothiatechs.customnotification.action.play_audio";
    String NEXT_ACTION = "com.marothiatechs.customnotification.action.next";
    String STARTFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.startforeground";
    String STOPFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.stopforeground";

    int FOREGROUND_SERVICE = 101;


}
